#include <iostream>
#include <thread>
#include <chrono>
using namespace std;

void threadFunction(int n) {
    for(int i = 0; i < n; i++) {
        cout << i << " " << std::this_thread::get_id << "   " << fflush;
    }
}

int main()
{
    std::thread(threadFunction, 100).detach();
    std::thread(threadFunction, 100).detach();
    std::thread(threadFunction, 100).join();
}
