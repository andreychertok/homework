
#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
using namespace std;

vector<int> v1;
int sum = 0;


void thr1() {
    int a;
    while (cin >> a) {
        v1.push_back(a);
    }
}


void thr2() {
    for (int i = 0; 1; i = min(v1.size(), (size_t) i + 1)) {
        if (i < v1.size()) {
            for (int j = 0; j < i + 1; j++) {
                sum += v1[j];
            }
            cout << sum << endl;
            sum = 0;
        }
    }

}
int main() {
    thread Thr1(thr1);
    thread Thr2(thr2);
    Thr1.join();
    Thr2.join();
}
